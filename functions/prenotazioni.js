module.exports = {
    controllaPrenotazioni: function(stanza, giorno, ora, res) {
        var sql_stmt = "SELECT id, risorsa FROM timetable WHERE stanza = '" + stanza + "' AND giorno = '" + giorno + "' AND ora = " + ora + ";";

        connection.query(sql_stmt, function(err, rows, fields) {
            if (!err) {
                try {
                    var id1 = rows[0].id;
                    var classe = rows[0].risorsa;
                    sql_stmt = "SELECT id FROM prenotazioni WHERE id = " + id1;

                    connection.query(sql_stmt, function(err, rows, fields) {
                        if (!err) {
                            try {
                                var id2 = rows[0].id;
                                sql_stmt = "SELECT who FROM prenotazioni WHERE id = " + id2;

                                connection.query(sql_stmt, function(err, rows, fields) {
                                    if (!err) {
                                        var username = rows[0].who;
                                        sql_stmt = "DELETE FROM prenotazioni WHERE id = " + id2;

                                        connection.query(sql_stmt, function(err) {
                                            if (!err) {
                                                mailFunctions.sendMailPrenotazioneRimossa(stanza, giorno, ora, username, classe);
                                                res(true);
                                            } else {
                                                res(false);
                                            }
                                        });
                                    }
                                });
                            } catch (e) {
                                res(true);
                            }
                        }
                    });
                } catch (err) {
                    return res(false);
                }
            } else {
                return res(false);
            }
        });
    },

    cancellaPrenotazione: function(stanza, giorno, ora, username, classe, res) {
        try {
            var sql_stmt = "UPDATE timetable SET risorsa = Null, professore1 = Null, professore2 = Null " +
                "WHERE stanza = '" + stanza + "' AND ora = " + ora + " AND giorno = '" + giorno + "';";

            connection.query(sql_stmt, function(err) {
                if (!err) {
                    utilsFunctions.selectId(giorno, stanza, ora, function(id) {
                        sql_stmt = "DELETE FROM prenotazioni WHERE id = " + id;

                        connection.query(sql_stmt, function(err) {
                            if (!err) {
                                mailFunctions.sendMailPrenotazioneRimossa(stanza, giorno, ora, username, classe);
                                res(true);
                            } else {
                                return res(false);
                            }
                        });
                    });
                } else {
                    return res(false);
                }
            });
        } catch (e) {
            return res(false);
        }
    },

    addPrenotazione: function(giorno, stanza, ora, risorsa, who, isOraScuola, admin, res) {

        var sql_stmt = "UPDATE timetable SET risorsa = '" + risorsa + "' WHERE stanza = '" +
            stanza + "' AND ora = " + ora + " AND giorno = '" + giorno + "';";

        connection.query(sql_stmt, function(err) {
            if (!err) {
                utilsFunctions.selectId(giorno, stanza, ora, function(id) {
                    sql_stmt = "INSERT INTO prenotazioni VALUES(" + id + ", '" + who + "', " + isOraScuola + ", " + admin + ");";
                    connection.query(sql_stmt, function(err) {
                        if (!err) {
                            res(true);
                        } else {
                            res(false);
                        }
                    });
                })
            } else {
                res(false);
            }
        });


    }
};