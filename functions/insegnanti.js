module.exports = {
    getProf1: function (stanza, giorno, ora, res) {
        var sql_stmt = "SELECT professore1 FROM timetable WHERE stanza = '" + stanza + "' AND giorno = '"
            + giorno + "' AND ora = " + ora + ";";
    
        connection.query(sql_stmt, function (err, rows, fields) {
            if (!err) {
                try {
                    var prof1 = rows[0].professore1;
                    res(prof1);
                } catch (err) {
                    return res(false)
                }
            } else {
                return res(false);
            }
        });
    },

    getProf2: function (stanza, giorno, ora, res) {
        var sql_stmt = "SELECT professore2 FROM timetable WHERE stanza = '" + stanza + "' AND giorno = '"
            + giorno + "' AND ora = " + ora + ";";
    
        connection.query(sql_stmt, function (err, rows, fields) {
            if (!err) {
                try {
                    var prof2 = rows[0].professore2;
                    res(prof2);
                } catch (err) {
                    return res(false);
                }
            } else {
                return res(false);
            }
        });
    },

    moveProfessori: function (stanza, stanzaDaLib, giorno, ora, res) {
        getProf1(stanzaDaLib, giorno, ora, function (response1) {
            prof1 = response1;
            getProf2(stanzaDaLib, giorno, ora, function (response2) {
                prof2 = response2;
                var sql_stmt = "UPDATE timetable SET professore1 = '" + prof1 + "' WHERE ora = " + ora +
                    " AND stanza = '" + stanza + "' AND giorno = '" + giorno + "';";
    
                connection.query(sql_stmt, function (err) {
                    if (!err) {
                        try {
                            var appo = prof2.split(" ")[0];
                            var sql_stmt = "UPDATE timetable SET professore2 = '" + prof2 + "' WHERE ora = " + ora +
                                " AND stanza = '" + stanza + "' AND giorno = '" + giorno + "';";
    
                            connection.query(sql_stmt, function (err) {
                                if (!err) {
                                    res(true);
                                } else {
                                    return res(false);
                                }
                            });
                        } catch (e) {
                            var sql_stmt = "UPDATE timetable SET professore2 = Null WHERE ora = " + ora +
                                " AND stanza = '" + stanza + "' AND giorno = '" + giorno + "';";
    
                            connection.query(sql_stmt, function (err) {
                                if (!err) {
                                    res(true);
                                } else {
                                    return res(false);
                                }
                            });
                        }
                    } else {
                        return res(false);
                    }
                });
            });
        });
    },

    getProfFromOrario: function (stanza, giorno, ora, risorsa, res) {
        var prof1;
        var prof2;
    
        var sql_stmt = "SELECT giorno_settimana FROM timetable WHERE stanza = '" + stanza + "' AND giorno = '"
            + giorno + "' AND ora = " + ora + ";";
    
        connection.query(sql_stmt, function (err, rows, fields) {
            if (!err) {
                var week_day;
    
                try {
                    week_day = rows[0].giorno_settimana;
                } catch (err) {
                    return res(false);
                }
    
                sql_stmt = "SELECT `Column 2` AS prof FROM GPU001 WHERE `Column 5` = " + week_day + " AND `Column 6` = '"
                    + ora + "' AND `Column 1` = '" + risorsa + "';";
    
                connection.query(sql_stmt, function (err, rows1, fields) {
                    if (!err) {
                        var p1 = rows1[0].prof;
    
                        try {
                            var p2 = rows1[1].prof;
                            sql_stmt = "SELECT `Column 1` AS nomeCognome FROM GPU004 WHERE `Column 0` = '" + p2 + "'";
    
                            connection.query(sql_stmt, function (err, rows, fields) {
                                if (!err) {
                                    prof2 = rows[0].nomeCognome;
                                    sql_stmt = "SELECT `Column 1` AS nomeCognome FROM GPU004 WHERE `Column 0` = '" + p1 + "'";
    
                                    connection.query(sql_stmt, function (err, rows, fields) {
                                        if (!err) {
                                            prof1 = rows[0].nomeCognome;
                                            sql_stmt = "UPDATE timetable SET professore1 = '" + prof1 + "', professore2 = '" + prof2 +
                                                "' WHERE stanza = '" + stanza + "' AND ora = " + ora + " AND giorno = '" + giorno + "'";
                                            res(sql_stmt);
                                        } else {
                                            return res(false);
                                        }
                                    });
                                } else {
                                    return res(false);
                                }
                            });
                        } catch (e) {
                            var prof2 = "Null";
                            sql_stmt = "SELECT `Column 1` AS nomeCognome FROM GPU004 WHERE `Column 0` = '" + p1 + "'";
    
                            connection.query(sql_stmt, function (err, rows, fields) {
                                if (!err) {
                                    prof1 = rows[0].nomeCognome;
                                    sql_stmt = "UPDATE timetable SET professore1 = '" + prof1 + "', professore2 = '" + prof2 +
                                        "' WHERE stanza = '" + stanza + "' AND ora = " + ora + " AND giorno = '" + giorno + "'";
                                    res(sql_stmt);
                                } else {
                                    return res(false);
                                }
                            });
                        }
                    } else {
                        return res(false);
                    }
                });
            } else {
                return res(false);
            }
        });
    },
    
    profInEventi: function (classi, id, giorno, ora) {
        var vett = classi.split(",");
    
        for (i in vett) {
            insegnantiFunctions.profToEventi(id, vett[i], ora, giorno);
        }
    },

    profToEventi: function (id, classe, ora, giorno) {
        var sql_stmt = "";
        var professori = "";
        var prof1, prof2 = "";
        sql_stmt = "SELECT professore1, professore2 FROM timetable WHERE risorsa = '" + classe +
            "' AND ora = " + ora + " AND giorno = '" + giorno + "'";
    
        connection.query(sql_stmt, function (err, rows, fields) {
            if (!err) {
                try {
                    professori += rows[0].professore1 + ", ";
                    professori += rows[0].professore2 + ", ";
                    professori = professori.replace("null,", "");
                    var sql_stmt = "INSERT INTO prof_eventi VALUES(" + id + ", " + ora + ", '" + professori + "')";
                    connection.query(sql_stmt);;
                } catch (e) {
                    professori = professori.replace("null, ", "");
                    var sql_stmt = "INSERT INTO prof_eventi VALUES(" + id + ", " + ora + ", '" + professori + "')";
                    connection.query(sql_stmt);
                }
            } else {
                professori = professori.replace("null, ", "");
                var sql_stmt = "INSERT INTO prof_eventi VALUES(" + id + ", " + ora + ", '" + professori + "')";
                connection.query(sql_stmt);
            }
        });
    }
};
