module.exports = {
    selectId: function (giorno, stanza, ora, res) {
        try {
            var sql_stmt = "SELECT id FROM timetable WHERE stanza = '" + stanza + "' AND ora = " + ora + " AND giorno = '" + giorno + "';";
    
            connection.query(sql_stmt, function (err, rows, fields) {
                if (!err) {
                    try {
                        res(rows[0].id);
                    } catch (err) {
                        return res(false);
                    }
                } else {
                    return res(false);
                }
            });
        } catch (err) {
            return res(false);
        }
    },

    isSchoolOur: function (giorno, stanza, ora, risorsa, res) {
        var sql_stmt = "SELECT giorno_settimana FROM timetable WHERE stanza = '" + stanza + "' AND giorno = '"
            + giorno + "' AND ora = " + ora + ";";
    
        connection.query(sql_stmt, function (err, rows, fields) {
            if (!err) {
                var week_day;
    
                try {
                    week_day = rows[0].giorno_settimana;
                } catch (err) {
                    return res(false);
                }
    
                sql_stmt = "SELECT `Column 4` AS oldStanza FROM GPU001 WHERE `Column 5` = " + week_day + " AND `Column 6` = '"
                    + ora + "' AND `Column 1` = '" + risorsa + "';";
    
                connection.query(sql_stmt, function (err, rows, fields) {
                    if (!err) {
                        try {
                            var oldStanza = rows[0].oldStanza;
                            sql_stmt = "SELECT stanza FROM timetable WHERE ora = " + ora + " AND  giorno = '"
                                + giorno + "' AND risorsa = '" + risorsa + "';";
    
                            connection.query(sql_stmt, function (err, rows, fields) {
                                if (!err) {
                                    try {
                                        var stanzaDaLib = rows[0].stanza;
    
                                        insegnantiFunctions.moveProfessori(stanza, stanzaDaLib, giorno, ora, function (resp) {
                                            if (resp) {
                                                utilsFunctions.liberaRisorse(stanzaDaLib, ora, giorno, function (response) {
                                                    res(response);
                                                });
                                            } else {
                                                return res(false);
                                            }
                                        })
                                    } catch (e) {
                                        insegnantiFunctions.getProfFromOrario(stanza, giorno, ora, risorsa, function (resp) {
                                            if (resp == false) {
                                                return res(false);
                                            } else {
                                                sql_stmt = resp;
                                                connection.query(sql_stmt, function (err) {
                                                    if (!err) {
                                                        res(true);
                                                    } else {
                                                        return res(false);
                                                    }
                                                });
                                            }
                                        });
                                    }
                                } else {
                                    return res(false);
                                }
                            });
                        } catch (e) {
                            return res(false);
                        }
                    } else {
                        return res(false);
                    }
                });
            } else {
                return res(false);
            }
        });
    },

    liberaRisorse: function (stanza, ora, giorno, res) {
        controllaPrenotazioni(stanza, giorno, ora, function (response) {
            var sql_stmt = "UPDATE timetable SET risorsa = Null, professore1 = Null, professore2 = Null " +
                "WHERE stanza = '" + stanza + "' AND ora = " + ora + " AND giorno = '" + giorno + "';";
    
            connection.query(sql_stmt, function (err) {
                if (!err) {
                    res(true);
                } else {
                    res(false);
                }
            });
        });
    },
    
    undoClasse: function (stanza, giorno, ora, risorsa, username, res) {
        var sql_stmt = "SELECT giorno_settimana FROM timetable WHERE stanza = '" + stanza + "' AND giorno = '"
            + giorno + "' AND ora = " + ora + ";";
    
        connection.query(sql_stmt, function (err, rows, fields) {
            if (!err) {
                var week_day;
    
                try {
                    week_day = rows[0].giorno_settimana;
                } catch (err) {
                    return res(false);
                }
    
                sql_stmt = "SELECT `Column 4` AS stanzaOrario FROM GPU001 WHERE `Column 5` = " + week_day + " AND `Column 6` = '"
                    + ora + "' AND `Column 1` = '" + risorsa + "';";
    
                connection.query(sql_stmt, function (err, rows, fields) {
                    if (!err) {
                        var stanzaOrario;
    
                        try {
                            stanzaOrario = rows[0].stanzaOrario;
                        } catch (err) {
                            return res(false);
                        }
    
                        sql_stmt = "SELECT risorsa FROM timetable WHERE stanza = '" +
                            stanzaOrario + "' AND ora = " + ora + " AND giorno = '" + giorno + "';";
    
                        connection.query(sql_stmt, function (err, rows, fields) {
                            if (!err) {
                                var classe;
    
                                try {
                                    classe = rows[0].risorsa;
                                } catch (err) {
                                    return res(false);
                                }
    
                                if (classe != null) {
                                    mailFunctions.sendMailSenzaAula(risorsa, giorno, ora, username);
                                    res(true);
                                } else {
                                    var sql_stmt = "UPDATE timetable SET risorsa = '" + risorsa + "' WHERE stanza = '" +
                                        stanzaOrario + "' AND ora = " + ora + " AND giorno = '" + giorno + "';";
    
                                    connection.query(sql_stmt, function (err) {
                                        if (!err) {
                                            insegnantiFunctions.moveProfessori(stanzaOrario, stanza, giorno, ora, function (response) {
                                                res(response);
                                            });
                                        } else {
                                            return res(false);
                                        }
                                    });
                                }
                            } else {
                                return res(false);
                            }
                        });
                    } else {
                        return res(false);
                    }
                });
            } else {
                return res(false);
            }
        });
    },

    hourToEndHour: function (giorno, data, res) {
        var schoolOur = 0;
    
        var vett = giorno.split("-");
        var year = vett[0];
        var month = vett[1];
        var day = vett[2];
    
        vett = data.split(":");
        var hour = vett[0];
        var minute = vett[1];
    
        var dt = new Date(year, month, day, hour, minute, 0);
    
        if (dt >= new Date(year, month, day, "8", "00") && dt <= new Date(year, month, day, "9", "00")) {
            schoolOur = 1;
        } else if (dt > new Date(year, month, day, "9", "00") && dt <= new Date(year, month, day, "10", "00")) {
            schoolOur = 2;
        } else if (dt > new Date(year, month, day, "10", "00") && dt <= new Date(year, month, day, "11", "00")) {
            schoolOur = 3;
        } else if (dt > new Date(year, month, day, "11", "00") && dt <= new Date(year, month, day, "12", "00")) {
            schoolOur = 4;
        } else if (dt > new Date(year, month, day, "12", "00") && dt <= new Date(year, month, day, "13", "00")) {
            schoolOur = 5;
        } else if (dt > new Date(year, month, day, "13", "00") && dt <= new Date(year, month, day, "14", "30")) {
            schoolOur = 6;
        } else if (dt > new Date(year, month, day, "14", "30") && dt <= new Date(year, month, day, "15", "30")) {
            schoolOur = 7;
        } else if (dt > new Date(year, month, day, "15", "30") && dt <= new Date(year, month, day, "16", "30")) {
            schoolOur = 8;
        } else if (dt > new Date(year, month, day, "16", "30") && dt <= new Date(year, month, day, "17", "30")) {
            schoolOur = 9;
        } else if (dt > new Date(year, month, day, "17", "30") && dt <= new Date(year, month, day, "18", "30")) {
            schoolOur = 10;
        }
    
        res(schoolOur);
    },

    hourToStartHour: function (giorno, data, res) {
        var schoolOur = 0;
    
        var vett = giorno.split("-");
        var year = vett[0];
        var month = vett[1];
        var day = vett[2];
    
        vett = data.split(":");
        var hour = vett[0];
        var minute = vett[1];
    
        var dt = new Date(year, month, day, hour, minute, 0);
    
        if (dt >= new Date(year, month, day, "8", "00") && dt < new Date(year, month, day, "9", "00")) {
            schoolOur = 1;
        } else if (dt >= new Date(year, month, day, "9", "00") && dt < new Date(year, month, day, "10", "00")) {
            schoolOur = 2;
        } else if (dt >= new Date(year, month, day, "10", "00") && dt < new Date(year, month, day, "11", "00")) {
            schoolOur = 3;
        } else if (dt >= new Date(year, month, day, "11", "00") && dt < new Date(year, month, day, "12", "00")) {
            schoolOur = 4;
        } else if (dt >= new Date(year, month, day, "12", "00") && dt < new Date(year, month, day, "13", "00")) {
            schoolOur = 5;
        } else if (dt >= new Date(year, month, day, "13", "00") && dt < new Date(year, month, day, "14", "30")) {
            schoolOur = 6;
        } else if (dt >= new Date(year, month, day, "14", "30") && dt < new Date(year, month, day, "15", "30")) {
            schoolOur = 7;
        } else if (dt >= new Date(year, month, day, "15", "30") && dt < new Date(year, month, day, "16", "30")) {
            schoolOur = 8;
        } else if (dt >= new Date(year, month, day, "16", "30") && dt < new Date(year, month, day, "17", "30")) {
            schoolOur = 9;
        } else if (dt >= new Date(year, month, day, "17", "30") && dt < new Date(year, month, day, "18", "30")) {
            schoolOur = 10;
        }
    
        res(schoolOur);
    },

    liberazione: function (id, classe, ora, giorno, username) {
        var sql_stmt = "SELECT stanza FROM timetable WHERE risorsa = '" + classe + "' AND ora = " + ora + " AND giorno = '" + giorno + "'";
        var prof1;
        var prof2;
        var professori;
        var vett = [];
    
        connection.query(sql_stmt, function (err, rows, fields) {
            if (!err) {
                try {
                    stanza = rows[0].stanza;
                    vett.push(stanza);
                    sql_stmt = "SELECT professore1, professore2 FROM timetable WHERE ora = " + ora +
                        " AND risorsa = '" + classe + "' AND giorno = '" + giorno + "'";
    
                    connection.query(sql_stmt, function (err, rows, fields) {
                        if (!err) {
                            try {
                                prof1 = rows[0].professore1;
                                prof2 = rows[0].professore2;
                                professori = prof1 + ", " + prof2;
                                professori = professori.replace(", null", "");
    
                                sql_stmt = "INSERT INTO prof_liberazione VALUES(" + id + ", " + ora + ", '" + professori + "')";
                                connection.query(sql_stmt);
    
                                for (i in vett) {
                                    utilsFunctions.liberaRisorse(stanza, ora, giorno, function (res) {
    
                                    });
                                }
                            } catch (e) {
                                prof2 = "";
                                professori = prof1 + ", " + prof2;
                                professori = professori.replace(", null", "");
    
                                sql_stmt = "INSERT INTO prof_liberazione VALUES(" + id + ", " + ora + ", '" + professori + "')";
                                connection.query(sql_stmt);
    
                                for (i in vett) {
                                    utilsFunctions.liberaRisorse(stanza, ora, giorno, function (res) {
    
                                    });
                                }
                            }
                        }
                    });
                } catch (e) {
                    try {
                        sql_stmt = "INSERT INTO prof_liberazione VALUES(" + id + ", " + ora + ", NULL)";
    
                        connection.query(sql_stmt, function (err, rows, fields) {
                            if (err) {
                                console.log("ERRORE");
                            }
                        });
                    } catch (e) {
                        //caso liberazione rieffettuata
                    }
                }
            }
        });
    },

    decodeUser: function (token, res) {
        // decode token
        if (token) {
            // verifies secret and checks exp
            jwt.verify(token, app.get('secret'), function (err, decoded) {
                if (err) {
                    res(false);
                } else {
                    // if everything is good, save to request for use in other routes
    
                    var user = {
                        username: decoded.username,
                        admin: decoded.admin
                    };
    
                    res(user);
                }
            });
        } else {
            // if there is no token
            // return an error
            return res(false);
        }
    },
    
    verifyRoomIsEmpty: function (giorno, ora, stanza, callback) {
        var sql = "SELECT risorsa FROM timetable WHERE giorno='" + giorno + "' AND ora=" + ora + " AND stanza='" + stanza + "'";
        connection.query(sql, function (err, rows, fields) {
            if (!err) {
                try {
                    return (rows[0].risorsa || rows[0].risorsa != null) ? callback(false) : callback(true);
                } catch (err) {
                    return callback(false);
                }
            } else {
                return callback(false);
            }
        });
    },
    
    getTTFromStanza: function (stanza, giorno, obj, res) {
        var sql_stmt = "SELECT giorno, ora, stanza, risorsa, giorno_settimana FROM timetable WHERE stanza = '" + stanza + "' AND giorno = '" + giorno + "' ORDER BY ora * 1"
        var vett = [];
    
        connection.query(sql_stmt, function (err, rows, fields) {
            if (!err) {
                if (!rows.length) {
                    obj[stanza] = vett;
                    res(true);
                }
    
                for (i in rows) {
                    rows[i].giorno = dateFormat(rows[i].giorno, "yyyy-mm-dd");
                    vett.push(rows[i]);
    
                    if (vett.length == rows.length) {
                        obj[stanza] = vett;
                        res(true);
                    }
                }
            }
        });
    }
};