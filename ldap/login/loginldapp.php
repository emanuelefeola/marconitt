<?php
error_reporting(E_ALL);
ini_set('display_errors', 'On');

// define('DOMAIN_FQDN', 'mahabharata.local');
// define('LDAP_SERVER', '192.168.19.94');

define('DOMAIN_FQDN', 'edu.marconi');
define('LDAP_SERVER', '172.16.1.10');


if (isset($_POST['submit']))
{
    $user = strip_tags($_POST['username']) .'@'. DOMAIN_FQDN;
    $pass = stripslashes($_POST['password']);

    $conn = ldap_connect("ldap://". LDAP_SERVER ."/");

    if (!$conn)
        $err = 'Could not connect to LDAP server';

    else
    {
        define('LDAP_OPT_DIAGNOSTIC_MESSAGE', 0x0032);

        ldap_set_option($conn, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($conn, LDAP_OPT_REFERRALS, 0);

        $bind = @ldap_bind($conn, $user, $pass);

        ldap_get_option($conn, LDAP_OPT_DIAGNOSTIC_MESSAGE, $extended_error);

        if (!empty($extended_error))
        {
            $errno = explode(',', $extended_error);
            $errno = $errno[2];
            $errno = explode(' ', $errno);
            $errno = $errno[2];
            $errno = intval($errno);

            if ($errno == 532)
                $err = 'Unable to login: Password expired';
        }

        elseif ($bind)
        {
            $base_dn = "OU=NTDocentiGroup,DC=". join(',DC=', explode('.', DOMAIN_FQDN));
            //$base_dn = "OU=5CI,OU=STUDENTI,DC=". join(',DC=', explode('.', DOMAIN_FQDN));
			// $base_dn = "ou=users, DC=". join(',DC=', explode('.', DOMAIN_FQDN));
            $result = ldap_search(array($conn,$conn), $base_dn, "(cn=*)");

            if (!count($result))
                $err = 'Unable to login1: '. ldap_error($conn);

            else
            {
                //$csv = fopen("./new.csv", "w+");
                //var_dump($csv);

                foreach ($result as $res)
                {
                    $info = ldap_get_entries($conn, $res);


                    for ($i = 0; $i < $info['count']; $i++)
                    {//                    echo "<hr>";print_r($info[$i]);
                		//echo "<hr />";
                        //var_dump($info[$i]);
                        //echo explode('@',$info[$i]['userprincipalname'][0])[0].";".$info[$i]['physicaldeliveryofficename'][0].";".$info[$i]['sn'][0].";".$info[$i]['givenname'][0];
                        //fwrite($csv, explode('@',$info[$i]['userprincipalname'][0])[0].";".$info[$i]['physicaldeliveryofficename'][0].";".$info[$i]['sn'][0].";".$info[$i]['givenname'][0]."\n");

                        if (isset($info[$i]['userprincipalname']) AND strtolower($info[$i]['userprincipalname'][0]) == strtolower($user))
                        {
                            session_start();
                            //var_dump($info[$i]['mail']);
                            $username = explode('@', $user);
                            $_SESSION['foo'] = 'bar';
                            $_SESSION['name'] = $info[$i]['givenname'][0];
                            $_SESSION['surname'] = $info[$i]['sn'][0];
                            // $_SESSION['username'] = explode('@',$info[$i]['userprincipalname'][0])[0];
                            // $_SESSION['mail'] = $info[$i]['mail'][0];

                            // set session variables...

                            //break;
                        }
                    }
                }

                //fclose($csv);
            }
        }
    }

    // session OK, redirect to home page
    if (isset($_SESSION['foo']))
    {
        //header('Location: /');
        //exit();
        var_dump($_SESSION);
    }

    elseif (!isset($err)) {
        $err = 'Unable to login2: '. ldap_error($conn);
        var_dump($_SESSION);
    }

    ldap_close($conn);
}
?>
<!DOCTYPE html><head><title>Login from Debian</title></head>
<style>
* { font-family: Calibri, Tahoma, Arial, sans-serif; }
.errmsg { color: red; }
#loginbox { font-size: 12px; }
</style>
<body>
<div style="margin:10px 0;"></div>
<div title="Login" style="width:400px" id="loginbox">
    <div style="padding:10px 0 10px 60px">
    <form action="loginldapp.php" id="login" method="post">
        <table><?php if (isset($err)) echo '<tr><td colspan="2" class="errmsg">'. $err .'</td></tr>'; ?>
            <tr>
                <td>Login (edu.marconi):</td>
                <td><input type="text" name="username" style="border: 1px solid #ccc;" autocomplete="off"/></td>
            </tr>
            <tr>
                <td>Password:</td>
                <td><input type="password" name="password" style="border: 1px solid #ccc;" autocomplete="off"/></td>
            </tr>
        </table>
        <input class="button" type="submit" name="submit" value="Login" />
    </form>
    </div>
</div>
</div>
</body></html>