<?php
error_reporting(E_ALL);
ini_set('display_errors', 'On');

// define('DOMAIN_FQDN', 'mahabharata.local');
// define('LDAP_SERVER', '192.168.19.94');

define('DOMAIN_FQDN', 'edu.marconi');
define('LDAP_SERVER', '172.16.1.10');


if (isset($_POST['username']) && isset($_POST['password']))
{
    $user = strip_tags($_POST['username']) .'@'. DOMAIN_FQDN;
    $pass = stripslashes($_POST['password']);

    $conn = ldap_connect("ldap://". LDAP_SERVER ."/");

    if (!$conn)
        $err = 'Could not connect to LDAP server';

    else
    {
        //define('LDAP_OPT_DIAGNOSTIC_MESSAGE', 0x0032);

        ldap_set_option($conn, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($conn, LDAP_OPT_REFERRALS, 0);

        $bind = @ldap_bind($conn, $user, $pass);

        //$bind ? echo 'true' : echo 'false';
        if ($bind)
            echo 'true';
        else
            echo 'false';
    }

    ldap_close($conn);
}
?>