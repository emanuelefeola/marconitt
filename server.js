/**
 * IMPORT modules and functions
 */
global.mailFunctions = require('./functions/mail.js');
global.prenotazioniFunctions = require('./functions/prenotazioni.js');
global.utilsFunctions = require('./functions/utils.js');
global.insegnantiFunctions = require('./functions/insegnanti.js');

global.express = require('express');
global.app = express();
global.bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

global.morgan = require('morgan');
global.http = require('http');
global.Converter = require("csvtojson").Converter;
global.mysql = require('mysql');
global.dateFormat = require('dateformat');
global.jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
global.config = require('./config_default'); // get our config file
global.sendmail = require('sendmail')();
global.querystring = require('querystring');

/**
 * IMPORT routes
 */
var infoGeneric = require('./routes/infoGeneric');
var infoByDayRouter = require('./routes/infoByDay');
var infoByDateRouter = require('./routes/infoByDate');
var infoByMonthRouter = require('./routes/infoByMonth');
var apiRouter = require('./routes/apiRoutes');

/**
 * SERVER configuration
 */
global.port = process.env.PORT || 8080; // used to create, sign, and verify tokens

global.connection = mysql.createConnection({
    host: config.db_host,
    user: config.db_user,
    password: config.db_password,
    database: config.db_name
});
connection.connect();

app.set('secret', config.secret); // secret variable

app.use(express.static(__dirname + '/web'));

app.use(morgan('dev'));

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use('/', infoGeneric);
app.use('/', infoByDayRouter);
app.use('/', infoByDateRouter);
app.use('/', infoByMonthRouter);
app.use('/api', apiRouter);

/**
 * SERVER start
 */
app.listen(port);
console.log('Magic happens at http://localhost:' + port);