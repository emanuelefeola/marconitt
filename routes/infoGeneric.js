const infoGeneric = express.Router();

// (GET http://localhost:8080/classesbyteacher)
infoGeneric.route('/classesbyteacher')
    .get((req, res, next) => {
        var teacher = req.query.teacher;
        var sql_stmt = "SELECT distinct `column 1` AS c1 FROM `GPU001` WHERE `column 2`='" + teacher + "' AND `column 1` NOT IN ('', 'RIC', 'D1', 'ALT') ORDER BY `column 1`";
        var vett = [];

        connection.query(sql_stmt, function (err, rows, fields) {
            if (!err) {
                if (!rows.length)
                    return res.json(vett);

                for (i in rows) {
                    vett.push(rows[i].c1);

                    if (vett.length == rows.length) {
                        res.json(vett);
                    }
                }
            }
        });
    });

// (GET http://localhost:8080/isholiday)
infoGeneric.route('/isholiday')
    .get((req, res, next) => {
        var day = req.query.day;
        var sql_stmt = "SELECT * FROM timetable WHERE giorno='" + day + "'";

        connection.query(sql_stmt, function (err, rows, fields) {
            if (!err) {
                if (rows.length === 0 | rows.length == undefined) {
                    res.json(true);
                } else {
                    res.json(false);
                }
            } else {
                res.json(false);
            }
        });
    });

// (GET http://localhost:8080/default)
infoGeneric.route('/default')
    .get((req, res, next) => {
        var object = {};
        var sql_stmt = "SELECT DISTINCT `Column 1` AS c1 FROM `GPU001` where `Column 1` NOT IN ('', 'RIC', 'D1', 'ALT', 'PRO') ORDER BY `Column 1`"
        var vett = [];

        connection.query(sql_stmt, function (err, rows, fields) {
            if (!err) {
                if (!rows.length)
                    return res.json('Errore: La tabella \'GPU001\' è vuota');

                for (i in rows) {
                    vett.push(rows[i].c1);

                    if (vett.length == rows.length) {
                        object["classes"] = vett;
                        sql_stmt = "SELECT DISTINCT b.`Column 1` AS c1 FROM `GPU001` a INNER JOIN `GPU004` b ON a.`Column 2` = b.`Column 0` ORDER BY b.`Column 1`";

                        connection.query(sql_stmt, function (err, rows, fields) {
                            if (!err) {
                                if (!rows.length)
                                    return res.json('Errore: La tabella \'GPU001\' è vuota');

                                vett = [];

                                for (i in rows) {
                                    vett.push(rows[i].c1);

                                    if (vett.length == rows.length) {
                                        object["teachers"] = vett;
                                        sql_stmt = "SELECT * FROM aule ORDER BY aula";

                                        connection.query(sql_stmt, function (err, rows, fields) {
                                            if (!err) {
                                                if (!rows.length)
                                                    return res.json('Errore: La tabella \'aule\' è vuota');

                                                vett = [];

                                                for (i in rows) {
                                                    vett.push(rows[i].aula);

                                                    if (vett.length == rows.length) {
                                                        object["rooms"] = vett;
                                                        sql_stmt = "SELECT DISTINCT progetto FROM progetti ORDER BY progetto";

                                                        connection.query(sql_stmt, function (err, rows, fields) {                                                            

                                                            if (!err) {
                                                                if (!rows.length)
                                                                    return res.json('Errore: La tabella \'progetti\' è vuota');

                                                                vett = [];

                                                                for (i in rows) {
                                                                    vett.push(rows[i].progetto);

                                                                    if (vett.length == rows.length) {
                                                                        object["projects"] = vett;
                                                                        sql_stmt = "SELECT * FROM aule_eventi ORDER BY aula";

                                                                        connection.query(sql_stmt, function (err, rows, fields) {
                                                                            if (!err) {
                                                                                if (!rows.length)
                                                                                    return res.json('Errore: La tabella \'aule_eventi\' è vuota');

                                                                                vett = [];

                                                                                for (i in rows) {
                                                                                    vett.push(rows[i].aula);

                                                                                    if (vett.length == rows.length) {
                                                                                        object["eventsrooms"] = vett;
                                                                                        sql_stmt = "SELECT * FROM aule where aula like 'L%' ORDER BY aula";

                                                                                        connection.query(sql_stmt, function (err, rows, fields) {
                                                                                            if (!err) {
                                                                                                if (!rows.length)
                                                                                                    return res.json('Errore: La tabella \'aule\' non contiene laboratori.');

                                                                                                vett = [];

                                                                                                for (i in rows) {
                                                                                                    vett.push(rows[i].aula);

                                                                                                    if (vett.length == rows.length) {
                                                                                                        object["labs"] = vett;
                                                                                                        sql_stmt = "SELECT * FROM aule where aula like 'A%' ORDER BY aula";

                                                                                                        connection.query(sql_stmt, function (err, rows, fields) {
                                                                                                            if (!err) {
                                                                                                                if (!rows.length)
                                                                                                                    return res.json('Errore: La tabella \'aule\' è vuota');

                                                                                                                vett = [];

                                                                                                                for (i in rows) {
                                                                                                                    vett.push(rows[i].aula);

                                                                                                                    if (vett.length == rows.length) {
                                                                                                                        object["classrooms"] = vett;
                                                                                                                        res.json(object);
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        });
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        });
                                                                                    }
                                                                                }
                                                                            }
                                                                        });
                                                                    }
                                                                }
                                                            }
                                                        });
                                                    }
                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        });
                    }
                }

            }
        });
    });

module.exports = infoGeneric;