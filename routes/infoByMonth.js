const infoByMonth = express.Router();

// (GET http://localhost:8080/eventscountbymonth)
infoByMonth.route('/eventscountbymonth')
    .get((req, res, next) => {
        var month = req.query.month;
        var sql_stmt = "SELECT giorno, COUNT(giorno) as quantity FROM `eventi` WHERE month(giorno) = " + month + " GROUP BY giorno";
        var vett = [];

        connection.query(sql_stmt, function (err, rows, fields) {
            if (!err) {
                if (!rows.length)
                    return res.json(vett);

                for (i in rows) {
                    rows[i].giorno = dateFormat(rows[i].giorno, "yyyy-mm-dd");
                    vett.push(rows[i]);

                    if (vett.length == rows.length) {
                        res.json(vett);
                    }
                }
            }
        });
    });

module.exports = infoByMonth;