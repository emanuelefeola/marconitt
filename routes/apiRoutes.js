const apiRoutes = express.Router();

// (GET http://localhost:8080/api/)
// (POST http://localhost:8080/api/)
apiRoutes.route('/')
    .get((req, res, next) => {
        res.json({ message: 'Welcome to the coolest API on earth!' });
    })
    .post((req, res, next) => {
        res.json({ message: 'Welcome to the coolest API on earth!' });
    });

// route to authenticate a user (POST http://localhost:8080/api/authenticate)
apiRoutes.route('/authenticate')
    .post((req, res, next) => {
        //query to search if user is in db
        var sql_stmt = "SELECT admin FROM users WHERE username ='" + req.body.name + "'";
        connection.query(sql_stmt, function(err, rows, fields) {
            if (!err) {
                try {
                    var adm = rows[0].admin;

                    var admin = adm === 0 ? false : true;
                    var user = {
                        username: req.body.name,
                        password: req.body.password,
                        admin: admin
                    };

                    var postData;
                    try {
                        postData = querystring.stringify({
                            'username': req.body.name,
                            'password': req.body.password
                        });
                    } catch (e) {
                        console.log('Errore: ' + e);
                    }

                    /*
                    var options;
                    try {
                        options = {
                            hostname: config.webserver,
                            port: 80,
                            path: './ldap/login/loginldap_marconitt.php',
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded',
                                'Content-Length': Buffer.byteLength(postData)
                            }
                        };
                    } catch(e) {
                        console.log(e);
                    }
                                        
                    const request = http.request(options, (result) => {
                        result.setEncoding('utf8');
                        result.on('data', (chunk) => {    
                            if (chunk == 'true') {
                                var token = jwt.sign(user, app.get('secret'), {
                                    expiresInMinutes: 1440 // expires in 24 hours
                                });
                                // return the information including token as JSON
                                res.json({
                                    success: true,
                                    message: 'Enjoy your token!',
                                    token: token,
                                    username: user.username,
                                    admin: user.admin
                                });
                            } else {
                                res.json({ success: false, message: 'Credenziali errate.' });
                            }
                        });

                        result.on('end', () => {
                            console.log('No more data in response.');
                        });
                    });
                    
                    request.on('error', (e) => {
                        //console.error(`problem with request: ${e.message}`);
                    });

                    // write data to request body
                    request.write(postData);
                    request.end();*/
                    var token = jwt.sign(user, app.get('secret'), {
                        expiresInMinutes: 1440 // expires in 24 hours
                    });
                    res.json({
                        success: true,
                        message: 'Enjoy your token!',
                        token: token,
                        username: user.username,
                        admin: true
                    });
                } catch (e) {
                    console.log(e);
                    res.json({ success: false, message: 'Errore di Login.' });
                }
            }
        });
    });


// (POST http://localhost:8080/api/mailsender)
apiRoutes.route('/mailsender')
    .post((req, res, next) => {
        var user = req.body.username;
        var stanza = req.body.stanza;
        var ora = req.body.ora;

        //sistemo la data
        var giorno = req.body.giorno;
        var parts = giorno.split("-");
        var mydate = new Date(parts[0], parts[1] - 1, parts[2]);
        var day = mydate.getDate();
        var mese = mydate.getMonth() + 1;
        var anno = mydate.getFullYear()

        sql_stmt = "SELECT mail FROM users WHERE username = '" + user + "'";

        connection.query(sql_stmt, function(err, rows, fields) {
            if (!err) {
                var pretesto = "La prenotazione da lei richiesta:  <br> Aula: " + stanza + " <br> Ora: " + ora + "° ora <br> Giorno: " + day + "-" + mese + "-" + anno + " <br> è stata annullata causa cambio dell'orario";
                var testo = '<!doctype html><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><title>Marconi TT</title><style type="text/css">.ReadMsgBody{width: 100%; background-color: #ffffff;}.ExternalClass{width: 100%; background-color: #ffffff;}body{width: 100%; background-color: #ffffff; margin:0; padding:0; -webkit-font-smoothing: antialiased;font-family: Georgia, Times, serif}table{border-collapse: collapse;}@media only screen and (max-width: 640px){body[yahoo] .deviceWidth{width:440px!important; padding:0;}body[yahoo] .center{text-align: center!important;}}@media only screen and (max-width: 479px){body[yahoo] .deviceWidth{width:280px!important; padding:0;}body[yahoo] .center{text-align: center!important;}}</style></head><body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" yahoo="fix" style="font-family: Georgia, Times, serif"><table width="600" style="margin-top:20px;" border="0" cellpadding="0" cellspacing="0" align="center"><tr bgcolor="#eeeeed"><td width="100%" valign="top" style="padding-top:20px"><table width="580" class="deviceWidth" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#eeeeed" style="margin:0 auto;"><tr><div style="height:15px;margin:0 auto;">&nbsp;</div><br></tr><tr><td valign="top" style="padding:0" bgcolor="#eeeeed"><center><img class="deviceWidth" src="http://i.imgur.com/1cSHWao.png" height="115" width="220" alt="logo" alkformat="srcU" border="0" style="display: block; border-radius: 4px;"/></center></td></tr><tr height="20px"></tr><tr> <td style="font-size: 20px; color: #000000; font-weight: normal; text-align: center; font-family: Georgia, Times, serif; line-height: 30px; vertical-align: top; padding:10px 8px 10px 8px" bgcolor="#eeeeed"> ' + pretesto + ' </td></tr><tr><td bgcolor="#409ea8" style="padding:5px 0;background-color:#409ea8; border-top:1px solid #77d5ea; background-repeat:repeat-x" align="center"></td></tr></table></td></tr><tr><td><table bgcolor="#ffffff" width="600" cellpadding="0" cellspacing="0" border="0" align="center"><tr><br></tr><tr bgcolor="#eeeeed"><td><table cellpadding="0" cellspacing="0" border="0" align="center" width="580" class="container"><tr><td width="80%" height="70" valign="middle" align="center" style="padding-bottom:10px;padding-top:10px; border-top-style:solid; border-top-color:#979FA3"><div class="contentEditableContainer contentTextEditable"><div align="center" style="margin-top:0px; font-size:13px;color:#181818;font-family:Helvetica, Arial, sans-serif;line-height:200%;text-align:center;"> Copyright © 2017. All right reserved to Marconi TT team.<br></div></div></td></tr></table></td></tr><tr ><td height="50" valign="middle" style="padding-bottom:10px;"></td></tr></table></td></tr></table> <div style="display:none; white-space:nowrap; font:15px courier; color:#ffffff;">- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -</div></body></html>';
                var oggettomail = 'MARCONI TT: prenotazione cancellata';
                mail = rows[0].mail;
                mailFunctions.sendMail("eliasemprebon98@gmail.com", testo, oggettomail);
                res.json(true);
            }
        });
    });

// (POST http://localhost:8080/api/verifyToken)
apiRoutes.route('/verifyToken')
    .post((req, res, next) => {
        var token = req.body.token || req.query.token || req.headers['x-access-token'];
        // decode token
        if (token) {
            // verifies secret and checks exp
            jwt.verify(token, app.get('secret'), function(err, decoded) {
                if (err) {
                    return res.json({ success: false, message: 'Failed to authenticate token.' });
                } else {
                    // if everything is good, save to request for use in other routes
                    req.decoded = decoded;
                    res.json({
                        success: true,
                        username: decoded.username,
                        admin: decoded.admin
                    });
                    next();
                }
            });
        } else {
            // if there is no token
            // return an error
            return res.status(403).send({
                success: false,
                message: 'No token provided.'
            });
        }
    });

// route middleware to verify a token
// methods declared after this call require token has to be passed
apiRoutes.use(function(req, res, next) {
    // check header or url parameters or post parameters for token
    var token = req.body.token || req.query.token || req.headers['x-access-token'];

    // decode token
    if (token) {
        // verifies secret and checks exp
        jwt.verify(token, app.get('secret'), function(err, decoded) {
            if (err) {
                return res.json({ success: false, message: 'Failed to authenticate token.' });
            } else {
                // if everything is good, save to request for use in other routes
                req.decoded = decoded;
                next();
            }
        });
    } else {
        // if there is no token
        // return an error
        return res.status(403).send({
            success: false,
            message: 'No token provided.'
        });
    }
});

// (POST http://localhost:8080/api/prenota)
apiRoutes.route('/prenota')
    .post((req, res, next) => {
        utilsFunctions.decodeUser(req.body.token, function (userVerified) {
            if (userVerified) {
                var stanza = req.body.stanza;
                var giorno = req.body.giorno;
                var ora = req.body.ora;
                var risorsa = req.body.risorsa;
                //var isClasse = (req.body.isclasse == "true");
                var isClasse;
                var user = userVerified.username; //req.body.user;
                var admin = userVerified.admin; //req.body.admin;
                var today = new Date(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate());

                utilsFunctions.verifyRoomIsEmpty(giorno, ora, stanza, function (roomIsEmpty) {
                    if (roomIsEmpty) {

                        var sql_stmt = "SELECT DISTINCT `Column 1` FROM `GPU001` where `Column 1` NOT IN ('', 'RIC', 'D1', 'ALT', 'PRO') AND `Column 1`='" + risorsa + "' ORDER BY `Column 1`";
                        connection.query(sql_stmt, function(err, rows, fields) {
                            if (!err) {
                                isClasse = rows[0] ? true : false;
                                if ((userVerified.admin || (!userVerified.admin && isClasse)) && new Date(giorno) >= today) {
                                    if (isClasse) {
                                        utilsFunctions.isSchoolOur(giorno, stanza, ora, risorsa, function (response) {
                                            prenotazioniFunctions.addPrenotazione(giorno, stanza, ora, risorsa, user, response, admin, function (response1) {
                                                res.json(response1);
                                            });
                                        });
                                    } else {
                                        prenotazioniFunctions.addPrenotazione(giorno, stanza, ora, risorsa, user, false, admin, function (response) {
                                            res.json(response);
                                        });
                                    }
                                } else {
                                    return res.json(false);
                                }
                            } else {
                                return res.json(false);
                            }
                        });
                    } else {
                        return res.json(false);
                    }
                });
            } else {
                return res.json(false);
            }
        });
    });

// (POST http://localhost:8080/api/approva)
apiRoutes.route('/approva')
    .post((req, res, next) => {
        utilsFunctions.decodeUser(req.body.token, function (userVerified) {
            if (userVerified.admin) {
                var id = req.body.id;

                var sql_stmt = "UPDATE prenotazioni SET approvata = True WHERE id = " + id;

                connection.query(sql_stmt, function(err, rows, fields) {
                    if (!err) {
                        var sql_stmt = "SELECT stanza, giorno, ora, who, risorsa FROM timetable INNER JOIN prenotazioni ON timetable.id=prenotazioni.id WHERE timetable.id=" + id;
                        connection.query(sql_stmt, function(err, rows, fields) {
                            if (!err) {
                                try {
                                    mailFunctions.sendMailPrenotazioneApprovata(rows[0].stanza, rows[0].giorno, rows[0].ora, rows[0].who, rows[0].risorsa);
                                    return res.json(true);
                                } catch (err) {
                                    return res.json(false);
                                }
                            } else {
                                return res.json(false);
                            }
                        });
                    } else {
                        return res.json(false);
                    }
                });
            } else {
                return res.json(false);
            }
        });
    });
// (POST http://localhost:8080/api/getPrenotazioniExceptAdmin)
apiRoutes.route('/getPrenotazioniExceptAdmin')
    .post((req, res, next) => {
        utilsFunctions.decodeUser(req.body.token, function (userVerified) {
            if (userVerified.admin) {
                var sql = "SELECT timetable.id, giorno, stanza, risorsa, ora, approvata, GPU004.`column 1` as user, who FROM timetable INNER JOIN prenotazioni ON timetable.id=prenotazioni.id INNER JOIN users ON users.username=prenotazioni.who INNER JOIN GPU004 ON who=GPU004.`column 0` WHERE users.admin=false ORDER BY giorno DESC, ora";
                connection.query(sql, function(err, rows, fields) {
                    for (i in rows)
                        rows[i].giorno = dateFormat(rows[i].giorno, "yyyy-mm-dd");

                    res.json(rows);
                });
            } else {
                return res.json(false);
            }
        });
    });

// (POST http://localhost:8080/api/getPrenotazioniAdmin)
apiRoutes.route('/getPrenotazioniAdmin')
    .post((req, res, next) => {
        utilsFunctions.decodeUser(req.body.token, function (userVerified) {
            if (userVerified.admin) {
                var sql = "SELECT timetable.id, giorno, stanza, risorsa, ora, approvata, who FROM timetable INNER JOIN prenotazioni ON timetable.id=prenotazioni.id INNER JOIN users ON users.username=prenotazioni.who WHERE users.admin=true ORDER BY giorno DESC, ora";
                connection.query(sql, function(err, rows, fields) {
                    if (!err) {
                        for (i in rows)
                            rows[i].giorno = dateFormat(rows[i].giorno, "yyyy-mm-dd");
                        
                        console.log(rows);
                        res.json(rows);
                    } else
                        return res.json(false);
                });
            } else {
                return res.json(false);
            }
        });
    });

// (POST http://localhost:8080/api/getEvents)
apiRoutes.route('/getEvents')
    .post((req, res, next) => {
        utilsFunctions.decodeUser(req.body.token, function (userVerified) {
            if (userVerified.admin) {
                var sql = "SELECT * FROM eventi ORDER BY giorno DESC";
                connection.query(sql, function(err, rows, fields) {
                    for (i in rows)
                        rows[i].giorno = dateFormat(rows[i].giorno, "yyyy-mm-dd");

                    res.json(rows);
                });
            } else {
                return res.json(false);
            }
        });
    });

// (POST http://localhost:8080/api/getPrenotazioniUser)
apiRoutes.route('/getPrenotazioniUser')
    .post((req, res, next) => {
        utilsFunctions.decodeUser(req.body.token, function (userVerified) {
            var sql = "SELECT timetable.id, giorno, stanza, risorsa, ora, approvata, who FROM timetable INNER JOIN prenotazioni ON timetable.id=prenotazioni.id WHERE who='" + userVerified.username + "' ORDER BY giorno DESC, ora";
            connection.query(sql, function(err, rows, fields) {
                res.json(rows);
            });
        });
    });

// (POST http://localhost:8080/api/cancellaPrenotazione)
apiRoutes.route('/cancellaPrenotazione')
    .post((req, res, next) => {
        var today = new Date(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate());
        var id = req.body.id;
        var verifyUserQuery = "SELECT stanza, giorno, ora, risorsa, who FROM timetable INNER JOIN prenotazioni ON prenotazioni.id=timetable.id WHERE timetable.id=" + id;

        connection.query(verifyUserQuery, function(err, rows, fields) {
            if (!err) {
                var stanza, giorno, ora, risorsa, username;
                try {
                    stanza = rows[0].stanza;
                    var nfGiorno = rows[0].giorno;
                    giorno = nfGiorno.getFullYear() + "-" + (nfGiorno.getMonth() + 1) + "-" + nfGiorno.getDate();
                    ora = rows[0].ora;
                    risorsa = rows[0].risorsa;
                    username = rows[0].who;

                } catch (err) {
                    return res.json(false);
                }

                utilsFunctions.decodeUser(req.body.token, function (userVerified) {
                    if ((userVerified.admin || userVerified.username === username) && new Date(giorno) >= today) {
                        sql_stmt = "SELECT isSchoolHour FROM prenotazioni WHERE id = " + id;

                        connection.query(sql_stmt, function(err, rows, fields) {
                            if (!err) {
                                var isSOur = rows[0].isSchoolHour;

                                if (isSOur) {
                                    utilsFunctions.undoClasse(stanza, giorno, ora, risorsa, username, function (response2) {
                                        if (response2) {
                                            prenotazioniFunctions.cancellaPrenotazione(stanza, giorno, ora, username, risorsa, function (response1) {
                                                return res.json(response1);
                                            })
                                        } else {
                                            return res.json(false);
                                        }
                                    });
                                } else {
                                    prenotazioniFunctions.cancellaPrenotazione(stanza, giorno, ora, username, risorsa, function (response3) {
                                        return res.json(response3);
                                    })
                                }
                            } else {
                                return res.json(false);
                            }
                        });
                    } else {
                        return res.json(false);
                    }
                });
            } else {
                return res.json(false);
            }
        });
    });

// (POST http://localhost:8080/api/creaEvento)
apiRoutes.route('/creaEvento')
    .post((req, res, next) => {
        utilsFunctions.decodeUser(req.body.token, function (userVerified) {
            if (userVerified.admin) {
                var oraInizio = 1;
                var oraFine = 0;
                var professori = "";
                var id;

                //Prendo le variabili dal form
                var descrizione = req.body.descrizione;
                var giorno = req.body.day;
                var start = req.body.oraInizio;
                var end = req.body.oraFine;
                var classi = req.body.classi;
                var stanze = req.body.stanze;

                //sistemo le ore
                utilsFunctions.hourToStartHour(giorno, start, function (res1) {
                    oraInizio = res1;

                    utilsFunctions.hourToEndHour(giorno, end, function (res1) {
                        oraFine = res1;

                        //inserisco l'evento
                        var sql_stmt = "INSERT INTO eventi(giorno, descrizione, oraInizio, oraFine, classi, stanze) VALUES('" + giorno + "', '" +
                            descrizione + "', '" + start + "', '" + end + "', '" + classi + "', '" + stanze + "')";

                        connection.query(sql_stmt, function(err) {
                            if (!err) {
                                res.json(true);
                                //seleziono l'id dell'evento inserito
                                sql_stmt = "SELECT id FROM eventi WHERE giorno = '" + giorno + "' AND descrizione = '" + descrizione +
                                    "' AND oraInizio = '" + start + "' AND oraFine = '" + end + "'";

                                connection.query(sql_stmt, function(err, rows, fields) {
                                    if (!err) {
                                        try {
                                            id = rows[0].id;
                                            cont = oraInizio;
                                            var vett = [];

                                            while (cont <= oraFine) {
                                                vett.push(cont);
                                                cont++;
                                            }
                                            cont = 0;
                                            for (ora in vett) {
                                                insegnantiFunctions.profInEventi(classi, id, giorno, vett[ora]);
                                            }
                                        } catch (err) {
                                            return res.json(false);
                                        }
                                    } else {
                                        return res.json(false);
                                    }
                                });
                            } else {
                                return res.json(false);
                            }
                        });
                    })
                })
            } else {
                return res.json(false);
            }
        });
    });

// (POST http://localhost:8080/api/cancellaEvento)
apiRoutes.route('/cancellaEvento')
    .post((req, res, next) => {
        utilsFunctions.decodeUser(req.body.token, function (userVerified) {
            if (userVerified.admin) {
                var id = req.body.id;
                var sql_stmt = "DELETE FROM prof_eventi WHERE id = " + id;

                connection.query(sql_stmt, function(err) {
                    if (!err) {
                        sql_stmt = "DELETE FROM eventi WHERE id = " + id;

                        connection.query(sql_stmt, function(err) {
                            if (!err) {
                                res.json(true);
                            } else {
                                return res.json(false);
                            }
                        });
                    } else {
                        return res.json(false);
                    }
                });
            }
        });
    });

// (POST http://localhost:8080/api/liberaRisorse)
apiRoutes.route('/liberaRisorse')
    .post((req, res, next) => {
        utilsFunctions.decodeUser(req.body.token, function (userVerified) {
            if (userVerified.admin) {
                var classe = req.body.classe;
                var s_ore = req.body.ore;
                var giorno = req.body.day;
                var descrizione = req.body.descrizione;
                var username = ''; // non inviamo mail per la liberazione delle risorse

                var ore = s_ore.split(",");
                var sql_stmt;
                var stanza;
                var id;

                sql_stmt = "INSERT INTO liberazione(giorno, descrizione, classe, ore) VALUES ('" + giorno + "', '" +
                    descrizione + "', '" + classe + "', '" + ore + "')";

                connection.query(sql_stmt, function(err) {
                    if (!err) {
                        //seleziono l'id della liberazione
                        sql_stmt = "SELECT id FROM liberazione WHERE giorno = '" + giorno + "' AND ore = '" + ore +
                            "' AND descrizione = '" + descrizione + "' AND classe = '" + classe + "'";

                        connection.query(sql_stmt, function(err, rows, fields) {
                            if (!err) {
                                id = rows[0].id;

                                for (var i in ore) {
                                    utilsFunctions.liberazione(id, classe, ore[i], giorno, username);
                                }
                                res.json(true);
                            } else {
                                return res.json(false);
                            }
                        });
                    } else {
                        return res.json(false);
                    }
                });
            } else {
                return res.json(false);
            }
        });
    });

// (POST http://localhost:8080/api/getPrenotazioniExceptAdmin)
apiRoutes.route('/getPrenotazioniExceptAdmin')
    .post((req, res, next) => {
        utilsFunctions.decodeUser(req.body.token, function (userVerified) {
            if (userVerified.admin) {
                var sql = "SELECT timetable.id, giorno, stanza, risorsa, ora, approvata, GPU004.`column 1` as user, who FROM timetable INNER JOIN prenotazioni ON timetable.id=prenotazioni.id INNER JOIN users ON users.username=prenotazioni.who INNER JOIN GPU004 ON who=GPU004.`column 0` WHERE users.admin=false ORDER BY giorno DESC, ora";
                connection.query(sql, function(err, rows, fields) {
                    for (i in rows)
                        rows[i].giorno = dateFormat(rows[i].giorno, "yyyy-mm-dd");

                    res.json(rows);
                });
            } else {
                return res.json(false);
            }
        });
    });

// (POST http://localhost:8080/api/getPrenotazioniAdmin)
apiRoutes.route('/getPrenotazioniExceptAdmin')
    .post((req, res, next) => {
        utilsFunctions.decodeUser(req.body.token, function (userVerified) {
            if (userVerified.admin) {
                var sql = "SELECT timetable.id, giorno, stanza, risorsa, ora, approvata, who FROM timetable INNER JOIN prenotazioni ON timetable.id=prenotazioni.id INNER JOIN users ON users.username=prenotazioni.who WHERE users.admin=true ORDER BY giorno DESC, ora";
                connection.query(sql, function(err, rows, fields) {
                    if (!err) {
                        for (i in rows)
                            rows[i].giorno = dateFormat(rows[i].giorno, "yyyy-mm-dd");

                        res.json(rows);
                    } else
                        return res.json(false);
                });
            } else {
                return res.json(false);
            }
        });
    });

// (POST http://localhost:8080/api/getEvents)
apiRoutes.route('/getEvents')
    .post((req, res, next) => {
        utilsFunctions.decodeUser(req.body.token, function (userVerified) {
            if (userVerified.admin) {
                var sql = "SELECT * FROM eventi ORDER BY giorno DESC";
                connection.query(sql, function(err, rows, fields) {
                    for (i in rows)
                        rows[i].giorno = dateFormat(rows[i].giorno, "yyyy-mm-dd");

                    res.json(rows);
                });
            } else {
                return res.json(false);
            }
        });
    });

// (POST http://localhost:8080/api/getPrenotazioniUser)
apiRoutes.route('/getPrenotazioniUser')
    .post((req, res, next) => {
        utilsFunctions.decodeUser(req.body.token, function (userVerified) {
            var sql = "SELECT timetable.id, giorno, stanza, risorsa, ora, approvata, who FROM timetable INNER JOIN prenotazioni ON timetable.id=prenotazioni.id WHERE who='" + userVerified.username + "' ORDER BY giorno DESC, ora";
            connection.query(sql, function(err, rows, fields) {
                res.json(rows);
            });
        });
    });

module.exports = apiRoutes;