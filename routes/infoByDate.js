const infoByDate = express.Router();

// (GET http://localhost:8080/classroombydate)
infoByDate.route('/classroomsbydate')
    .get((req, res, next) => {
        var giorno = req.query.day;
        var sql_stmt = "SELECT * FROM aule WHERE aule.aula like 'A%' ORDER BY aula";
        var obj = {};
        var cont = 0;

        connection.query(sql_stmt, function (err, rows, fields) {
            if (!err) {
                if (!rows.length)
                    return res.json(obj);

                for (i in rows) {
                    var stanza = rows[i].aula;

                    utilsFunctions.getTTFromStanza(stanza, giorno, obj, function (response) {
                        cont++;

                        if (cont == rows.length) {
                            res.json(obj);
                        }
                    });
                }
            }
        });
    });

// (GET http://localhost:8080/labroomsbydate)
infoByDate.route('/labroomsbydate')
    .get((req, res, next) => {
        var giorno = req.query.day;
        var sql_stmt = "SELECT * FROM aule WHERE aule.aula like 'L%' ORDER BY aula";
        var obj = {};
        var cont = 0;

        connection.query(sql_stmt, function (err, rows, fields) {
            if (!err) {
                if (!rows.length)
                    return res.json(obj);

                for (i in rows) {
                    var stanza = rows[i].aula;

                    utilsFunctions.getTTFromStanza(stanza, giorno, obj, function (response) {
                        cont++;

                        if (cont == rows.length) {
                            res.json(obj);
                        }
                    });
                }
            }
        });
    });

module.exports = infoByDate;