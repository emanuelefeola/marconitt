const infoByDay = express.Router();

// (GET http://localhost:8080/eventsbyday)
infoByDay.route('/eventsbyday')
    .get((req, res, next) => {
        var day = req.query.day;
        var sql_stmt = "SELECT * FROM eventi WHERE giorno='" + day + "' ORDER BY giorno";
        var vett = [];

        connection.query(sql_stmt, function (err, rows, fields) {
            if (!err) {
                if (!rows.length)
                    return res.json(vett);

                for (i in rows) {
                    rows[i].giorno = dateFormat(rows[i].giorno, "yyyy-mm-dd");
                    vett.push(rows[i]);

                    if (vett.length == rows.length) {
                        res.json(vett);
                    }
                }
            }
        });
    });

// (GET http://localhost:8080/ttroombyday)
infoByDay.route('/ttroombyday')
    .get((req, res, next) => {
        var day = req.query.day;
        var room = req.query.room;
        var sql_stmt = "select * from timetable where stanza = '" + room + "' and giorno = '" + day + "' order by ora";
        var vett = [];

        connection.query(sql_stmt, function (err, rows, fields) {
            if (!err) {
                if (!rows.length)
                    return res.json(vett);

                for (i in rows) {
                    rows[i].giorno = dateFormat(rows[i].giorno, "yyyy-mm-dd");
                    vett.push(rows[i]);

                    if (vett.length == rows.length) {
                        res.json(vett);
                    }
                }
            }
        });
    });

infoByDay.route('/liberazioniclassbyday')
    .get((req, res, next) => {
        var day = req.query.day;
        var classe = req.query.classe;
        var sql_stmt = "select liberazione.descrizione, prof_liberazione.professori, prof_liberazione.ora from liberazione inner join prof_liberazione on liberazione.id = prof_liberazione.liberazione where liberazione.classe = '" + classe + "' and giorno = '" + day + "' order by ora";
        var vett = [];

        connection.query(sql_stmt, function (err, rows, fields) {
            if (!err) {
                if (!rows.length)
                    return res.json(vett);

                for (i in rows) {
                    vett.push(rows[i]);

                    if (vett.length == rows.length) {
                        res.json(vett);
                    }
                }
            }
        });
    });

infoByDay.route('/teachereventsbyday')
    .get((req, res, next) => {
        var day = req.query.day;
        var teacher = req.query.prof;
        var sql_stmt = "select distinct eventi.descrizione, eventi.stanze, prof_eventi.ora from eventi inner join prof_eventi on eventi.id = prof_eventi.id where eventi.giorno = '" + day + "' and prof_eventi.professori like '%" + teacher + "%' order by oraInizio";
        var vett = [];

        connection.query(sql_stmt, function (err, rows, fields) {
            if (!err) {
                if (!rows.length)
                    return res.json(vett);

                for (i in rows) {
                    vett.push(rows[i]);

                    if (vett.length == rows.length) {
                        res.json(vett);
                    }
                }
            }
        });
    });

infoByDay.route('/ttteacherbyday')
    .get((req, res, next) => {
        var day = req.query.day;
        var teacher = req.query.prof;
        var sql_stmt = "select * from timetable where (professore1 = '" + teacher + "' or professore2 = '" + teacher + "') and giorno = '" + day + "' order by ora";
        var vett = [];

        connection.query(sql_stmt, function (err, rows, fields) {
            if (!err) {
                if (!rows.length)
                    return res.json(vett);

                for (i in rows) {
                    rows[i].giorno = dateFormat(rows[i].giorno, "yyyy-mm-dd");
                    vett.push(rows[i]);

                    if (vett.length == rows.length) {
                        res.json(vett);
                    }
                }
            }
        });
    });

infoByDay.route('/ttclassbyday')
    .get((req, res, next) => {
        var day = req.query.day;
        var classe = req.query.classe;
        var sql_stmt = "select * from timetable where risorsa = '" + classe + "' and giorno = '" + day + "' order by ora";
        var vett = [];

        connection.query(sql_stmt, function (err, rows, fields) {
            if (!err) {
                if (!rows.length)
                    return res.json(vett);

                for (i in rows) {
                    rows[i].giorno = dateFormat(rows[i].giorno, "yyyy-mm-dd");
                    vett.push(rows[i]);

                    if (vett.length == rows.length) {
                        res.json(vett);
                    }
                }
            }
        });
    });

infoByDay.route('/classeventsbyday')
    .get((req, res, next) => {
        var day = req.query.day;
        var classe = req.query.classe;
        var sql_stmt = "select eventi.descrizione, eventi.stanze, prof_eventi.ora, prof_eventi.professori from eventi inner join prof_eventi on eventi.id = prof_eventi.id where eventi.giorno = '" + day + "' and eventi.classi like '%" + classe + "%' order by oraInizio;";
        var vett = [];

        connection.query(sql_stmt, function (err, rows, fields) {
            if (!err) {
                if (!rows.length)
                    return res.json(vett);

                for (i in rows) {
                    vett.push(rows[i]);

                    if (vett.length == rows.length) {
                        res.json(vett);
                    }
                }
            }
        });
    });

module.exports = infoByDay;