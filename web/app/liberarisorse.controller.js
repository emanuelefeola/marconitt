app
    .controller('LiberaRisorseCtrl', function ($scope, $timeout, $http, $mdToast, $rootScope, CONFIG, $mdDialog) {

        $scope.classes; // list of classes
        $scope.event = {};
        $scope.event.description; // description of the event
        $scope.event.startDay = new Date(); // start day of the event
        $scope.event.endDay = new Date(); // end day of the event
        $scope.event.class; // selected class of the event
        $scope.event.ore; // selected hours of the event
        $scope.isLoading = true; // used for loading circle


        /**
         * Initialization method
         */
        var init = function () {
            $http.get('http://' + CONFIG.HOST + ':' + CONFIG.PORT + '/default')
                .success(function (response) {

                    $scope.classes = response.classes;
                    $timeout(function () {
                        $scope.isLoading = false;
                    }, $rootScope.loadingTime);

                }).error(function () {
                    $mdToast.show($mdToast.simple().textContent("Errore di rete!"));
                });
        };


        /**
         * sends event information to server
         */
        $scope.insert = function () {
            //control if selected date is not holiday
            var startDate = new Date($scope.event.startDay);
            var endDate = new Date($scope.event.endDay);

            if (startDate > endDate) {
                $mdToast.show($mdToast.simple().textContent("La data di fine non può precedere quella di fine."));

            } else {
                //date is not holiday
                var confirm = $mdDialog.confirm()
                    .textContent('La risorsa verrà liberata per sempre nei giorni selezionati. Continuare?')
                    .ok('CONFERMA')
                    .cancel('ANNULLA');

                $mdDialog.show(confirm).then(function () {
                    var startDate = new Date($scope.event.startDay);
                    var endDate = new Date($scope.event.endDay);    
                    endDate.setDate(endDate.getDate()+1);                
                    
                    while (startDate <= endDate) {
                        var desc = $scope.event.description;

                        if ($scope.noSunday(startDate)) {
                            console.log(startDate.getDate());
                            var giorno = startDate.getDate();
                            var mese = startDate.getMonth() + 1;
                            var anno = startDate.getFullYear();
                            var day = anno + "-" + mese + "-" + giorno;
    
                            var sClass = $scope.event.class;
                            var sOre = $scope.event.ore;
                            var data = "descrizione=" + desc + "&day=" + day + "&classe=" + sClass + "&ore=" + sOre + "&token=" + sessionStorage.token;
    
                            var req = {
                                method: 'POST',
                                url: 'http://' + CONFIG.HOST + ':' + CONFIG.PORT + '/api/liberaRisorse',
                                data: data,
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                }
                            };
    
                            $http(req)
                                .then(
                                    function (data) {
                                        if (data.data) {
                                            $mdToast.show($mdToast.simple().textContent("Risorsa liberata con successo"));
                                            $scope.event = {};
                                            $scope.event.startDay = new Date();
                                            $scope.event.endDay = new Date();
                                        } else {      
                                            var msg = "";                                      
                                            if (data.data.msg) {
                                                msg = ": "+data.data.msg;
                                            } 
                                            $mdToast.show($mdToast.simple().textContent("Errore durante la liberazione della risorsa"+msg));
                                        }
                                    },
                                    function (err) {
                                        $mdToast.show($mdToast.simple().textContent("Errore di rete!"));
                                    }
                                ); 
                        } else {
                            $mdToast.show($mdToast.simple().textContent("Non è possibile liberare una domenica!"));
                        }

                        startDate.setDate(startDate.getDate() + 1);
                    }
                });
            }

        };


        /**
         * says if a day is sunday or not
         * @param date
         * @returns {boolean}
         */
        $scope.noSunday = function (date) {
            var day = date.getDay();
            return day !== 0;
        };

        $scope.updateEndDate = function(dateIndex) {
            if (dateIndex == 1) {
                if ($scope.event.endDay < $scope.event.startDay)
                    $scope.event.endDay = $scope.event.startDay;
            } else {
                if ($scope.event.startDay > $scope.event.endDay)
                    $scope.event.startDay = $scope.event.endDay;
            }
            
        }

        // on start
        init();
    });